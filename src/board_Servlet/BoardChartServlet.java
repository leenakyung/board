package board_Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.google.gson.Gson;

@WebServlet("/BoardChartServlet")
public class BoardChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		Connection conn =null; 
	
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env"); 
			DataSource ds = (DataSource) envCtx.lookup("jdbc/Oracle11g");
			conn = ds.getConnection();
			
			Gson gson = new Gson();
			Map<String,String> totals = new HashMap<String,String>();
			
			totals.put("noFiletotal", noFilechart(conn));
			totals.put("filetotal", filetotalchart(conn));
			totals.put("oneWeek", oneWeek(conn));
			totals.put("twoWeek", twoWeek(conn));
			totals.put("threeWeek", threeWeek(conn));
			totals.put("fourWeek", fourWeek(conn));
			totals.put("fiveWeek", fiveWeek(conn));
			totals.put("sixWeek", sixWeek(conn));
			totals.put("oneWeekCount", oneWeekCount(conn));
			totals.put("twoWeekCount", twoWeekCount(conn));
			totals.put("threeWeekCount", threeWeekCount(conn));
			totals.put("fourWeekCount", fourWeekCount(conn));
			totals.put("fiveWeekCount", fiveWeekCount(conn));
			totals.put("sixWeekCount", sixWeekCount(conn));
			
			String jsonPlacetest = gson.toJson(totals);
			PrintWriter out = response.getWriter();
			out.write(jsonPlacetest);
			
			
			}catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception e) {
					}
				}
			}
		}
	//한달 전 파일이 없는 게시물 수	
	private String noFilechart(Connection conn) throws Exception {

		PreparedStatement noFilepstmt = null;
		ResultSet noFilers = null;
		String total = null;

		try {
			noFilepstmt = conn.prepareStatement("select count(*) from screen where (file1 is null) and (data >= add_months(sysdate,-1))");
			noFilers = noFilepstmt.executeQuery();
			while (noFilers.next()) {
				total = noFilers.getString(1);
			}

		} catch (Exception e) {
			
			throw e;
		} finally {
			if (noFilers != null) {
				try {
					noFilers.close();
				} catch (Exception e) {
				}
			}
			if (noFilepstmt != null) {
				try {
					noFilepstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return total;
	}
	//한달 전 파일이 있는 게시물 수	
	private String filetotalchart(Connection conn) throws Exception {

		PreparedStatement filetotalpstmt = null;
		ResultSet filetotalrs = null;
		String filetotal = null;

		try {

			filetotalpstmt = conn.prepareStatement("select count(*) from screen where (file1 is not null) and (data >= add_months(sysdate,-1))");
			filetotalrs = filetotalpstmt.executeQuery();
			
			while (filetotalrs.next()) {
				filetotal = filetotalrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (filetotalrs != null) {
				try {
					filetotalrs.close();
				} catch (Exception e) {
				}
			}
			if (filetotalpstmt != null) {
				try {
					filetotalpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return filetotal;
	}
	//한주 전 날짜 출력
	private String oneWeek(Connection conn) throws Exception {

		PreparedStatement oneWeekpstmt = null;
		ResultSet oneWeekrs = null;
		String oneWeek = null;
		try {
			oneWeekpstmt = conn.prepareStatement("select to_char(sysdate,'yyyy-mm-dd')from dual");
			oneWeekrs = oneWeekpstmt.executeQuery();
			
			while (oneWeekrs.next()) {
				oneWeek = oneWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (oneWeekrs != null) {
				try {
					oneWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (oneWeekpstmt != null) {
				try {
					oneWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return oneWeek;
	}
	//이주 전 날짜 출력
	private String twoWeek(Connection conn) throws Exception {

		PreparedStatement twoWeekpstmt = null;
		ResultSet twoWeekrs = null;
		String twoWeek = null;
		try {
			twoWeekpstmt = conn.prepareStatement("select to_char(sysdate-7,'yyyy-mm-dd')from dual");
			twoWeekrs = twoWeekpstmt.executeQuery();
			
			while (twoWeekrs.next()) {
				twoWeek = twoWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (twoWeekrs != null) {
				try {
					twoWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (twoWeekpstmt != null) {
				try {
					twoWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return twoWeek;
	}
	//삼주 전 날짜 출력
	private String threeWeek(Connection conn) throws Exception {

		PreparedStatement threeWeekpstmt = null;
		ResultSet threeWeekrs = null;
		String threeWeek = null;
		try {
			threeWeekpstmt = conn.prepareStatement("select to_char(sysdate-14,'yyyy-mm-dd')from dual");
			threeWeekrs = threeWeekpstmt.executeQuery();
			
			while (threeWeekrs.next()) {
				threeWeek = threeWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (threeWeekrs != null) {
				try {
					threeWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (threeWeekpstmt != null) {
				try {
					threeWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return threeWeek;
	}
	//사주 전 날짜 출력
	private String fourWeek(Connection conn) throws Exception {

		PreparedStatement fourWeekpstmt = null;
		ResultSet fourWeekrs = null;
		String fourWeek = null;
		try {
			fourWeekpstmt = conn.prepareStatement("select to_char(sysdate-21,'yyyy-mm-dd')from dual");
			fourWeekrs = fourWeekpstmt.executeQuery();
			
			while (fourWeekrs.next()) {
				fourWeek = fourWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (fourWeekrs != null) {
				try {
					fourWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (fourWeekpstmt != null) {
				try {
					fourWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return fourWeek;
	}
	//오주 전 날짜 출력
	private String fiveWeek(Connection conn) throws Exception {

		PreparedStatement fiveWeekpstmt = null;
		ResultSet fiveWeekrs = null;
		String fiveWeek = null;
		try {
			fiveWeekpstmt = conn.prepareStatement("select to_char(sysdate-28,'yyyy-mm-dd')from dual");
			fiveWeekrs = fiveWeekpstmt.executeQuery();
			
			while (fiveWeekrs.next()) {
				fiveWeek = fiveWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (fiveWeekrs != null) {
				try {
					fiveWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (fiveWeekpstmt != null) {
				try {
					fiveWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return fiveWeek;
	}
	//육주 전 날짜 출력
	private String sixWeek(Connection conn) throws Exception {

		PreparedStatement sixWeekpstmt = null;
		ResultSet sixWeekrs = null;
		String sixWeek = null;
		try {
			sixWeekpstmt = conn.prepareStatement("select to_char(sysdate-35,'yyyy-mm-dd')from dual");
			sixWeekrs = sixWeekpstmt.executeQuery();
			
			while (sixWeekrs.next()) {
				sixWeek = sixWeekrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (sixWeekrs != null) {
				try {
					sixWeekrs.close();
				} catch (Exception e) {
				}
			}
			if (sixWeekpstmt != null) {
				try {
					sixWeekpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return sixWeek;
	}
	//한주 전 게시물 수
	private String oneWeekCount(Connection conn) throws Exception {

		PreparedStatement oneWeekCountpstmt = null;
		ResultSet oneWeekCountrs = null;
		String oneWeekCount = null;
		try {
			oneWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data >= (sysdate-7)");
			oneWeekCountrs = oneWeekCountpstmt.executeQuery();
			
			while (oneWeekCountrs.next()) {
				oneWeekCount = oneWeekCountrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (oneWeekCountrs != null) {
				try {
					oneWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (oneWeekCountpstmt != null) {
				try {
					oneWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return oneWeekCount;
	}
	//이주 전 게시물 수
	private String twoWeekCount(Connection conn) throws Exception {

		PreparedStatement twoWeekCountpstmt = null;
		ResultSet twoWeekCountrs = null;
		String twoWeekCount = null;
		try {
			twoWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data between (sysdate-14) and (sysdate-7)");
			twoWeekCountrs = twoWeekCountpstmt.executeQuery();
			
			while (twoWeekCountrs.next()) {
				twoWeekCount = twoWeekCountrs.getString(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {

			if (twoWeekCountrs != null) {
				try {
					twoWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (twoWeekCountpstmt != null) {
				try {
					twoWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return twoWeekCount;
	}
	//삼주 전 게시물 수
	private String threeWeekCount(Connection conn) throws Exception {

		PreparedStatement threeWeekCountpstmt = null;
		ResultSet threeWeekCountrs = null;
		String threeWeekCount = null;
		try {
			threeWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data between (sysdate-21) and (sysdate-14)");
			threeWeekCountrs = threeWeekCountpstmt.executeQuery();
			
			while (threeWeekCountrs.next()) {
				threeWeekCount = threeWeekCountrs.getString(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {

			if (threeWeekCountrs != null) {
				try {
					threeWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (threeWeekCountpstmt != null) {
				try {
					threeWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return threeWeekCount;
	}
	//사주 전 게시물 수
	private String fourWeekCount(Connection conn) throws Exception {

		PreparedStatement fourWeekCountpstmt = null;
		ResultSet fourWeekCountrs = null;
		String fourWeekCount = null;
		try {
			fourWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data between (sysdate-28) and (sysdate-21)");
			fourWeekCountrs = fourWeekCountpstmt.executeQuery();
			
			while (fourWeekCountrs.next()) {
				fourWeekCount = fourWeekCountrs.getString(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {

			if (fourWeekCountrs != null) {
				try {
					fourWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (fourWeekCountpstmt != null) {
				try {
					fourWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return fourWeekCount;
	}
	//오주 전 게시물 수
	private String fiveWeekCount(Connection conn) throws Exception {

		PreparedStatement fiveWeekCountpstmt = null;
		ResultSet fiveWeekCountrs = null;
		String fiveWeekCount = null;
		try {
			fiveWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data between (sysdate-35) and (sysdate-28)");
			fiveWeekCountrs = fiveWeekCountpstmt.executeQuery();
			
			while (fiveWeekCountrs.next()) {
				fiveWeekCount = fiveWeekCountrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (fiveWeekCountrs != null) {
				try {
					fiveWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (fiveWeekCountpstmt != null) {
				try {
					fiveWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return fiveWeekCount;
	}
	private String sixWeekCount(Connection conn) throws Exception {

		PreparedStatement sixWeekCountpstmt = null;
		ResultSet sixWeekCountrs = null;
		String sixWeekCount = null;
		try {
			sixWeekCountpstmt = conn.prepareStatement("select count(*) from screen where data between (sysdate-42) and (sysdate-35)");
			sixWeekCountrs = sixWeekCountpstmt.executeQuery();
			
			while (sixWeekCountrs.next()) {
				sixWeekCount = sixWeekCountrs.getString(1);
			}

		} catch (Exception e) {
			throw e;
		} finally {

			if (sixWeekCountrs != null) {
				try {
					sixWeekCountrs.close();
				} catch (Exception e) {
				}
			}
			if (sixWeekCountpstmt != null) {
				try {
					sixWeekCountpstmt.close();
				} catch (Exception e) {
				}
			}
		}
		return sixWeekCount;
	}
}

